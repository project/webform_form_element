<?php
/**
 * @file
 * Webform module form component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_form() {
  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'private' => 0,
    'extra' => array(
      'module' => '',
      'file' => '',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_form($component) {
  $form = array();
  
  // Load the available view modes for 'node' entities, so users can also
  // choose custom ones if they have defined any.
  // Maybe RSS and search results shouldn't be listed. Maybe. Dunno.
  $view_modes = array();
  $data = entity_get_info('node');
  foreach ($data['view modes'] as $key => $value) {
    $view_modes[$key] = $value['label'];
  }
  
  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Form ID'),
    '#default_value' => $component['value'],
    '#description' => t('Enter a valid form_id, whose elements will be added to the webform.') . theme('webform_token_help'),
    '#weight' => -1,
    '#element_validate' => array('_webform_edit_validate_form'),
    '#required' => TRUE,
  );
  $form['extra']['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Module'),
    '#default_value' => $component['extra']['module'],
    '#description' => t('Enter the short name of the module that defines the form you want to include. For example: <em>node</em>'),
  );
  $form['extra']['file'] = array(
    '#type' => 'textfield',
    '#title' => t('File'),
    '#default_value' => $component['extra']['file'],
    '#description' => t('Enter the short name of the file, relative to the above module directory, that defines the form you want to include. For example: <em>node.pages.inc</em>'),
  );
  $form['extra']['description'] = array(
    '#markup' => t('Sometimes a form is defined in an extra file inthe module directory. In this case, enter the module short name and file that defines the form callback in the fields above. If the form is defined in the .module file, you should leave these fields blank.'),
  );
  
  return $form;
}

/**
 * Element validation callback. Ensure a valid node ID was entered.
 */
function _webform_edit_validate_form($element, &$form_state) {
  if (!empty($element['#value'])) {

    // Check for and load the include file.
    if (!empty($form_state['values']['extra']['module']) && !empty($form_state['values']['extra']['file'])) {
      $file = DRUPAL_ROOT . '/' . drupal_get_path('module', $form_state['values']['extra']['module']) . '/' . $form_state['values']['extra']['file'];

      if (!file_exists($file)) {
        form_error($element, t('The include file %file cannot be found in the %module module directory', array('%file' => $form_state['values']['extra']['file'], '%module' => $form_state['values']['extra']['module'])));
      }
      require_once $file;
    }

    // Check that the form exists.
    $form = drupal_get_form($element['#value']);
    if (empty($form['form_id'])) {
      form_error($element, t('Unknown form %form_id. A form ID consists of letter, numbers and underscores only.', array('%form' => $element['#value'])));
    }

  }
  return TRUE;
}
